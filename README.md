# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Usage

- Create React App to create skeleton react app
- Yup form validator
- Formik as form component
- Redux as state managment
- Bootstrap as css framework
- Typescript

## Description

The solution is split in a UI and SDK part

### UI

Containers which serves the survey
Components which serves the form

## SDK

Any state management and services are created in sdk folder

## Running the project

Run `npm install`
Run `npm start` and check http://localhost:3000

## Building the project

Run `npm build`
Run `serve -s build` Note: if you don't have `serve` module run `npm i -g serve`

## Screenshots

<img src="screenshots/identity.jpg" width="220">
<img src="screenshots/details.jpg" width="220">
<img src="screenshots/favourite.jpg" width="220">
<img src="screenshots/summary.jpg" width="220">

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
