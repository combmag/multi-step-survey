import { Provider } from 'react-redux';
import store from './sdk/store';
import Survey from './ui/containers/Survey';

function App() {
  return (
    <Provider store={store}>
      <Survey />
    </Provider>
  );
}

export default App;
