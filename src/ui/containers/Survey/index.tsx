import { Form, Formik } from "formik";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { STEPS } from "../../../sdk/survey/survey.model";
import { getSurvey } from "../../../sdk/survey/survey.selector";

import Identity from "../../components/Identity";
import { goNextStep, goPreviousStep, submitSurvey } from "../../../sdk/survey/survey.action";
import { identityFormField, identityValidationSchema } from "../../components/Identity/identity.schema";
import Details from "../../components/Details";
import { detailFormField, detailValidationSchema } from "../../components/Details/details.schema";
import { favouriteFormField, favouriteValidationSchema } from "../../components/Favourite/favourite.schema";
import Favourites from "../../components/Favourite";
import Summary from "../../components/Summary";

const Body: React.FC = () => {
    const surveyState = useSelector(getSurvey);
    switch (surveyState.currentStep) {
        case STEPS.IDENTITY:
            return <Identity formField={identityFormField} />;
        case STEPS.FAVORITES:
            return <Favourites formField={favouriteFormField} />;
        case STEPS.DETAILS:
            return <Details formField={detailFormField} />;
        case STEPS.SUMMARY:
            return <Summary />;
        default:
            return <React.Fragment />;
    }
}

const Survey: React.FC = () => {
    const dispatch = useDispatch();
    const surveyState = useSelector(getSurvey);


    const isLastStep = surveyState.currentStep === STEPS.SUMMARY;
    if (surveyState.submitted) {
        return <div>Sorry! Survey already Submitted</div>

    }
    const handleSubmit = (_: any, actions: any) => {
        if (isLastStep) {
            actions.setSubmitting(false);
            dispatch(submitSurvey());
        }
        else {
            dispatch(goNextStep());
            actions.setTouched({});
            actions.setSubmitting(false);
        }
    }
    const validationSchema = [
        identityValidationSchema,
        detailValidationSchema,
        favouriteValidationSchema
    ]
    const currentValidationSchema = validationSchema[surveyState.currentStep];
    return (
        <div className="modal d-block">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h3 className="modal-title">Survey</h3>
                    </div>
                    <div className="modal-body">
                        <Formik
                            initialValues={surveyState.surveyData}
                            validationSchema={currentValidationSchema}
                            onSubmit={handleSubmit}
                            className="modal d-block"
                        >
                            <Form id="surveyForm">
                                <Body />
                                <div className="modal-footer">
                                    {surveyState.currentStep !== 0 &&
                                        (<button type="button" onClick={() => dispatch(goPreviousStep())} className="btn btn-primary">Previous</button>)}
                                    <button type="submit" className="btn btn-primary" >{isLastStep ? "Submit" : "Next"}</button>
                                </div>
                            </Form>
                        </Formik>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Survey;