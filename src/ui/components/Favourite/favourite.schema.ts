import * as Yup from "yup";
import { FavouriteFormFields } from "./favourite.model";

export const favouriteFormField: FavouriteFormFields = {
    book: {
        name: 'book',
        label: 'Book Name',
        requiredErrorMsg: 'Book is required',
    },
    colours: {
        name: 'colours',
        requiredErrorMsg: 'Colour is required',
    }
}

export const favouriteValidationSchema =
    Yup.object().shape({
        [favouriteFormField.book.name]: Yup.string().required(favouriteFormField.book.requiredErrorMsg),
        [favouriteFormField.colours.name]: Yup.array().min(1, favouriteFormField.colours.requiredErrorMsg)
    })
