import { ErrorValidationSchema } from "../../../sdk/survey/survey.model";

export interface FavouriteFormFields {
    book: ErrorValidationSchema;
    colours: ErrorValidationSchema;
}