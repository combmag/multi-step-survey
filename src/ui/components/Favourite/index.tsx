import React from "react";

import { FavouriteFormFields } from "./favourite.model";
import InputField from "../FormFields/InputField";
import Checkbox from "../FormFields/Checkbox";
import { updateSurveyColors, updateSurveyData } from "../../../sdk/survey/survey.action";
import { useDispatch } from "react-redux";

interface Props {
    formField: FavouriteFormFields;
}

const Favourites: React.FC<Props> = (props) => {
    const colorList = ["Red", "Green", "Blue", "Black"];
    const dispatch = useDispatch();
    return (
        <React.Fragment>
            <h4>Favourite</h4>
            <div className="form-group">
                <label htmlFor="book">Favourite Book</label>
                <InputField blurHandler={(book) => dispatch(updateSurveyData({ book }))} name={props.formField.book.name} type="text" className="form-control" placeholder={props.formField.book.label} />
            </div>
            <div className="form-group">
                <label htmlFor="colours">Colors</label>
                <Checkbox handleInputChange={(colours => dispatch(updateSurveyColors(colours)))} list={colorList} name={props.formField.colours.name} />
            </div>
        </React.Fragment>
    )
}

export default Favourites;
