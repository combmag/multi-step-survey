import { ErrorValidationSchema } from "../../../sdk/survey/survey.model";

export interface DetailFormFields {
    age: ErrorValidationSchema;
    gender: ErrorValidationSchema;
}