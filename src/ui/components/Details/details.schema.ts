import * as Yup from "yup";
import { DetailFormFields } from "./details.model";

export const detailFormField: DetailFormFields = {
    age: {
        name: 'age',
        requiredErrorMsg: 'Age is required'
    },
    gender: {
        name: 'gender',
        requiredErrorMsg: 'Gender is required'
    }
}

export const detailValidationSchema =
    Yup.object().shape({
        [detailFormField.age.name]: Yup.string().required(detailFormField.age.requiredErrorMsg),
        [detailFormField.gender.name]: Yup.string().required(detailFormField.gender.requiredErrorMsg)
    })
