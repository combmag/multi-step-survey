import React from "react";
import { DetailFormFields } from "./details.model";
import Select from "../FormFields/Select";
import Radio from "../FormFields/Radio";
import { updateSurveyData } from "../../../sdk/survey/survey.action";
import { useDispatch } from "react-redux";

interface Props {
    formField: DetailFormFields;
}

// Detail Step
const Details: React.FC<Props> = (props) => {
    const genderList = ["Man", "Woman",  "Prefer not to respond"];
    const ageOptions: Record<string, string> = {
        "": "Select your age",
        "12-18": "12 - 18",
        "18-30": "18 - 30",
        "40-50": "40 - 50",
        "50-60": "50 - 60",
        "60+": "60+"
    }
    const dispatch = useDispatch();
    return (
        <React.Fragment>
            <h4>Details</h4>
            <div className="form-group">
                <Select optionList={ageOptions} handleSelectChange={(age => dispatch(updateSurveyData({age})))} name={props.formField.age.name} />
            </div>
            <div className="form-group">
                <label htmlFor="gender">Gender</label>
                <Radio handleSelectChange={(gender => dispatch(updateSurveyData({gender})))} itemList={genderList} name={props.formField.gender.name} className="form-control" />
            </div>
        </React.Fragment>
    )
}

export default Details;
