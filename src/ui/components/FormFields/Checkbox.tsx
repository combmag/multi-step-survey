import React from 'react';
import { at } from 'lodash';
import { FieldHookConfig, useField } from 'formik';

interface OwnProps {
    list: string[]
    handleInputChange: (value: string) => {};
}

type Props = OwnProps & FieldHookConfig<string> & React.DetailedHTMLProps<React.SelectHTMLAttributes<HTMLSelectElement>, HTMLSelectElement>

export default function Checkbox(props: Props) {
    const [field, meta] = useField(props.name);

    const renderHelperText = () => {
        const [touched, error] = at(meta, 'touched', 'error');
        if (touched && error) {
            return error;
        }
    }
    const isColorSelected = (color: string) => {
        return field.value.includes(color);
    };
    return (
        <div {...field}>
            {props.list.map((color) => (
                <div className="form-check mb-2" key={color}>
                    <input
                        className="form-check-input"
                        type="checkbox"
                        name={field.name}
                        value={color}
                        id={color}
                        checked={isColorSelected(color)}
                        onChange={(e => {
                            props.handleInputChange(e.target.value);
                        })}
                    />
                    <label className="form-check-label" htmlFor={color}>
                        {color}
                    </label>
                </div>
            ))}
            <span className="badge bg-danger">{renderHelperText()}</span>
        </div>
    );
}

