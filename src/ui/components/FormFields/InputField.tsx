import React from 'react';
import { at } from 'lodash';
import { FieldHookConfig, useField } from 'formik';

interface OwnProps {
    blurHandler: (e: string) => void;
    id?: string;
    placeholder?: string;
}

type Props = OwnProps & FieldHookConfig<string> & React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

// Generic input field
export default function InputField(props: Props) {
    const [field, meta] = useField(props);

    const renderHelperText = () => {
        const [touched, error] = at(meta, 'touched', 'error');
        if (touched && error) {
            return error;
        }
    }
    return (
        <React.Fragment>
            <input
                {...field}
                type="text"
                id={props.id}
                placeholder={props.placeholder}
                onBlur={e => {
                    props.blurHandler(e.target.value)
                }}
                className={`form-control ${renderHelperText() ? "is-invalid" : ""}`}
            />

            <span className="badge bg-danger">{renderHelperText()}</span>
        </React.Fragment>
    );
}