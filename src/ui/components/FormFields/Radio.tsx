import React from 'react';
import { at } from 'lodash';
import { FieldHookConfig, useField } from 'formik';

interface OwnProps {
    itemList: string[]
    handleSelectChange: (e: string) => {};
}

type Props = OwnProps & FieldHookConfig<string> & React.DetailedHTMLProps<React.SelectHTMLAttributes<HTMLSelectElement>, HTMLSelectElement>

// Multiselect Radio Component 
export default function RadioField(props: Props) {
    const [field, meta] = useField(props);

    const renderHelperText= () => {
        const [touched, error] = at(meta, 'touched', 'error');
        if (touched && error) {
            return error;
        }
    }
  return (
    <div {...field}>
      {props.itemList.map(item => (
        <div className="form-check" key={item}>
          <input
            className="form-check-input"
            type="radio"
            name="gender"
            id={item}
            value={item}
            onChange={(e => props.handleSelectChange(e.target.value))}
            checked={field.value === item}
          />
          <label className="form-check-label" htmlFor={item}>
            {item}
          </label>
        </div>
      ))}
      <span className="badge bg-danger">{renderHelperText()}</span>
    </div>
  );
}
