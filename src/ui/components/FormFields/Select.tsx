import React from 'react';
import { at } from 'lodash';
import { FieldHookConfig, useField } from 'formik';

interface OwnProps {
    handleSelectChange: (e: string) => {};
    optionList: Record<string, string>;
}
type Props = OwnProps& FieldHookConfig<string> & React.DetailedHTMLProps<React.SelectHTMLAttributes<HTMLSelectElement>, HTMLSelectElement>

// Component to create a dropdown select
export default function SelectField(props: Props) {
    const [field, meta] = useField(props);

    function renderHelperText() {
        const [touched, error] = at(meta, 'touched', 'error');
        if (touched && error) {
            return error;
        }
    }
    return (
        <React.Fragment>
             <select
                    {...field}
                    className="form-select"
                    onChange={e => {
                        props.handleSelectChange(e.target.value);
                        field.onChange(e);
                    }}
                >
                    {
                        Object.entries(props.optionList).map(([key,value]) => {
                            return <option key={key} value={key}>{value}</option>
                        })
                    }
                </select>
            <span className="badge bg-danger">{renderHelperText()}</span>
        </React.Fragment>
    );
}
