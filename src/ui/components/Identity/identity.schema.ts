import * as Yup from "yup";
import { IdentityFormFields } from "./identity.model";

export const identityFormField: IdentityFormFields = {
    firstName: {
        name: 'firstName',
        label: 'First name',
    },
    emailAddress: {
        name: 'emailAddress',
        label: 'someone@domain.com'
    }
}

export const identityValidationSchema =
    Yup.object().shape({
        [identityFormField.firstName.name]: Yup.string().notRequired(),
        [identityFormField.emailAddress.name]: Yup.string().notRequired()
    })
