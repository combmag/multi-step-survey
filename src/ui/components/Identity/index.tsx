import React from "react";
import { useDispatch } from "react-redux";
import { updateSurveyData } from "../../../sdk/survey/survey.action";
import InputField from "../FormFields/InputField";
import { IdentityFormFields } from "./identity.model";

interface Props {
    formField: IdentityFormFields;
}

const Identity: React.FC<Props> = ({ formField }) => {
    const dispatch = useDispatch();
    return (
        <React.Fragment>
            <div className="form-group">
                <h4>Identity</h4>
                <label htmlFor="firstName">Name</label>
                <InputField
                    blurHandler={(firstName => dispatch(updateSurveyData({ firstName })))}
                    name={formField.firstName.name}
                    id="firstName"
                    placeholder={formField.firstName.label}
                />
            </div>
            <div className="form-group">
                <label htmlFor="emailAddress">Email Address</label>
                <InputField 
                blurHandler={(emailAddress => dispatch(updateSurveyData({ emailAddress })))} 
                name={formField.emailAddress.name} 
                id="email" 
                placeholder={formField.emailAddress.label} />
            </div>
        </React.Fragment>
    )
}

export default Identity;