import { ErrorValidationSchema } from "../../../sdk/survey/survey.model";

export interface IdentityFormFields {
    firstName: ErrorValidationSchema;
    emailAddress: ErrorValidationSchema;
}