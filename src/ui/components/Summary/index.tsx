import React from "react";
import { useSelector } from "react-redux";
import { getSurveyData } from "../../../sdk/survey/survey.selector";

const Summary: React.FC = () => {
    const surveyData = useSelector(getSurveyData);
    return (
        <React.Fragment>
            <h4>Summary</h4>
            <div className="row">
                <div className="col-md-6">Name:</div>
                <div className="col-md-6">{surveyData.firstName}</div>
            </div>
            <div className="row">
                <div className="col-md-6">Email:</div>
                <div className="col-md-6">{surveyData.emailAddress}</div>
            </div>
            <div className="row">
                <div className="col-md-6">Age:</div>
                <div className="col-md-6">{surveyData.age}</div>
            </div>
            <div className="row">
                <div className="col-md-6">Gender:</div>
                <div className="col-md-6">{surveyData.gender}</div>
            </div>
            <div className="row">
                <div className="col-md-6">Favorite Book:</div>
                <div className="col-md-6">{surveyData.book}</div>
            </div>
            <div className="row">
                <div className="col-md-6"> Colors:</div>
                <ul className="col-md-6 list-group">
                    {surveyData.colours.map((color) => (
                        <li key={color} style={{ color: color }} className="col-md-6 list-group-item">
                            {color}
                        </li>
                    ))}
                </ul>
            </div>
        </React.Fragment>
    )
}

export default Summary;