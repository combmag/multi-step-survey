import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';

const APP_NAME = "survey-app";

// Create the Servey Element
function createSurveyContainer() {
  const surveyContainer = document.createElement("div");
  surveyContainer.setAttribute("id", APP_NAME);
  document.body.appendChild(surveyContainer);
}

function initApp() {
    window.onload = () => {
      createSurveyContainer();
      setTimeout(() => {
        // Open App after 2 seconds timeout
        ReactDOM.render(<App />, document.getElementById(APP_NAME));
      }, 2000);
    };
  }

initApp()

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
