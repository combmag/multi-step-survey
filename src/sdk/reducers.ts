import { combineReducers } from 'redux';
import { SurveyState } from './survey/survey.model';
import { surveyReducer } from './survey/survey.reducer';

export type RootState = {
    survey: SurveyState;
}

export const rootReducer = combineReducers({
    survey: surveyReducer,
})