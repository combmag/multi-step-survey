
import { compose, createStore } from 'redux'
import { rootReducer } from './reducers'
import { loadState, saveState } from './state/localstorage.service';

// Add redux dev tools part of window
declare global {
    interface Window {
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: Function
    }
  }

const composeEnhancers =
  (window && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose


const persistedState = loadState();
  
const store = createStore(
    rootReducer,
    // Load state from localstorage
    persistedState,
    composeEnhancers())

    store.subscribe(() => {
        saveState({
          survey: store.getState().survey
        });
      });
export default store