import { action, ActionType } from 'typesafe-actions';
import { SurveyActions, SurveyData } from './survey.model';

export const goNextStep = () =>
    action(SurveyActions.NEXT_STEP);

export const goPreviousStep = () =>
    action(SurveyActions.PREV_STEP);

export const updateSurveyData = (payload: Partial<SurveyData>) =>
    action(SurveyActions.UPDATE_DATA, {
        data: payload
    })

export const submitSurvey = () =>
    action(SurveyActions.SUBMIT_SURVEY);

export const updateSurveyColors = (colors: string) =>
    action(SurveyActions.UPDATE_COLORS, {
        colors
    })

export type SurveyActionTypes =
    ActionType<typeof updateSurveyData> &
    ActionType<typeof goPreviousStep> &
    ActionType<typeof goNextStep> &
    ActionType<typeof updateSurveyColors> &
    ActionType<typeof submitSurvey>