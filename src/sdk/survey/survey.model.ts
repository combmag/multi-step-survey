export interface SurveyState {
    currentStep: STEPS;
    surveyData: SurveyData;
    submitted: boolean;
}

export interface SurveyData {
    firstName: string;
    emailAddress: string;
    age: string;
    gender: string;
    book: string;
    colours: string[];
}


export enum STEPS {
    IDENTITY = 0,
    DETAILS = 1,
    FAVORITES = 2,
    SUMMARY = 3
}

export const SurveyActions = {
    NEXT_STEP: "@survey/NEXT_STEP",
    PREV_STEP: "@survey/PREV_STEP",
    UPDATE_DATA: "@survey/UPDATE_DATA",
    UPDATE_COLORS: "@survey/UPDATE_COLORS",
    SUBMIT_SURVEY: "@survey/SUBMIT_SURVEY"
}

export interface ErrorValidationSchema {
    name: string,
    label?: string;
    requiredErrorMsg?: string;
}