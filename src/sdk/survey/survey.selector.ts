import { RootState } from "../reducers";

export const getSurvey = (state: RootState) => state.survey;

export const getSurveyData = (state: RootState) => state.survey.surveyData;