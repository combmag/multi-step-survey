
import { STEPS, SurveyActions, SurveyState } from './survey.model';
import { SurveyActionTypes } from './survey.action';

const INITIAL_STATE: SurveyState = {
    currentStep: STEPS.IDENTITY,
    surveyData: {
        age: "",
        book: "",
        colours: [],
        emailAddress: "",
        gender: "",
        firstName: "",
    },
    submitted: false
}

export function surveyReducer(state: SurveyState = INITIAL_STATE, action: SurveyActionTypes): SurveyState {
    switch(action.type) {
        case SurveyActions.NEXT_STEP: {
            return {
                ...state,
                currentStep: state.currentStep  + 1
            }
        }
        case SurveyActions.UPDATE_DATA: {
            return {
                ...state,
                surveyData: {
                    ...state.surveyData,
                    ...action.payload.data
                }
            }
        }
        case SurveyActions.SUBMIT_SURVEY: {
            return {
                ...state,
                submitted: true
            }
        }
        case SurveyActions.UPDATE_COLORS: {
            const colour = action.payload.colors;
            let newColours = [...state.surveyData.colours, colour];
            if(state.surveyData.colours.includes(action.payload.colors)) {
                newColours = newColours.filter((color) => color !== colour);
            }
            return {
                ...state,
                surveyData: {
                    ...state.surveyData,
                    colours: newColours
                }
            }
        }
        case SurveyActions.PREV_STEP: {
            return {
                ...state,
                currentStep: state.currentStep  - 1
            }
        }
        default: {
            return state;
        }
    }
}